//
//  AlertController.swift
//  TaskManager
//
//  Created by Maksim on 27.03.2022.
//

import UIKit

class AlertController: UIAlertController {
    var doneButton = "Сохранить"
        
    func action(with taskList: TaskList?, completion: @escaping (String) -> Void) {
        
        if taskList != nil {
            doneButton = "Обновить"
        }
                
        let saveAction = UIAlertAction(title: doneButton, style: .default) { _ in
            guard let newValue = self.textFields?.first?.text else { return }
            guard !newValue.isEmpty else { return }
            completion(newValue)
        }
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .destructive)
        
        addAction(saveAction)
        addAction(cancelAction)
        addTextField { textField in
            textField.placeholder = "Имя списка"
            textField.text = taskList?.name
        }
    }
    
    func action(with task: Task?, completion: @escaping (String, String) -> Void) {
                        
        if task != nil {
            doneButton = "Обновить"
        }
        let saveAction = UIAlertAction(title: "Сохранить", style: .default) { _ in
            guard let newTask = self.textFields?.first?.text else { return }
            guard !newTask.isEmpty else { return }
            
            if let note = self.textFields?.last?.text, !note.isEmpty {
                completion(newTask, note)
            } else {
                completion(newTask, "")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .destructive)
        
        addAction(saveAction)
        addAction(cancelAction)
        
        addTextField { textField in
            textField.placeholder = "Новая задача"
            textField.text = task?.name
        }
        
        addTextField { textField in
            textField.placeholder = "Описание"
            textField.text = task?.note
        }
    }
}

